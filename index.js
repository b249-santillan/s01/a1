// ACTIVITY

// alert("Activity 1 - B249")

/*	QUIZ SECTION
	
1. How do you create arrays in JS?
	
	ANSWER:
		let arrayName = ["Element1", "Element2", ...]

2. How do you access the first character of an array?
	
	ANSWER:
		arrayName[0]

3. How do you access the last character of an array?
	
	ANSWER:
		arrayName[arrayName.length-1]

4. What array method searches for, and returns the index of a given value
in an array?
a. This method returns -1 if given value is not found in the array.
	
	ANSWER:
		indexOf("value")

5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
	
	ANSWER:
		forEach()

6. What array method creates a new array with elements obtained from a user-defined function?
	
	ANSWER:
		map()

7. What array method checks if all its elements satisfy a given condition?
	
	ANSWER:
		every()

8. What array method checks if at least one of its elements satisfies a given condition?
	
	ANSWER:
		some()

9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
	
	ANSWER:
		False

10. True or False: array.slice() copies elements from original array and returns these as a new array.
	
	ANSWER:
		True
*/


// FUNCTION CODING
	
	let students = ["John", "Joe", "Jane", "Jessie"]

	/*
		1. Create a function named addToEnd that will add a passed in string to
		the end of a passed in array. If element to be added is not a string,
		return the string "error - can only add strings to an array". Otherwise,
		return the updated array. Use the students array and the string "Ryan"
		as arguments when testing.
	*/

	// console.log(typeof(students))

	function addToEnd (arrayName, name)
	{
		if (typeof name === "string")
		{
			arrayName.push(name);
			console.log(students);
		}
		else
		{
			console.log(`error - can only add strings to an array`)
		}
	}

	console.log("#1: addToEnd")
	addToEnd(students, "Ryan");
	addToEnd(students, 045);

	/*
		2. Create a function named addToStart that will add a passed in string to
		the start of a passed in array. If element to be added is not a string, return
		the string "error - can only add strings to an array". Otherwise, return the
		updated array. Use the students array and the string "Tess" as arguments
		when testing.
	*/

	function addToStart (arrayName, name)
	{
		if (typeof name === "string")
		{
			arrayName.unshift(name);
			console.log(students);
		}
		else
		{
			console.log(`error - can only add strings to an array`)
		}
	}

	console.log("#2: addToStart")
	addToStart (students, "Tess");
	addToStart(students, 033);


	/*
		3. Create a function named elementChecker that will check a passed in
		array if at least one of its elements has the same value as a passed in
		argument. If array is empty, return the message "error - passed in array is
		empty". Otherwise, return a boolean value depending on the result of the
		check. Use the students array and the string "Jane" as arguments when
		testing.
	*/

	function elementChecker (arrayName, x)
	{
		if (arrayName.length !== 0)
		{
			console.log(arrayName.includes(x))
		}
		else 
		{
			console.log("error - passed in array is	empty")
		}
	}

	console.log("#3: elementChecker")
	elementChecker (students, "Jane");
	elementChecker ([], "Jane");


	/*
		4. Create a function named checkAllStringsEnding that will accept a passed in array and
		a character. The function will do the ff:
		● if array is empty, return "error - array must NOT be empty"
		● if at least one array element is NOT a string, return "error - all array elements must
		be strings"
		● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of
		data type string"
		● if 2nd argument is more than 1 character long, return "error - 2nd argument must be
		a single character"
		 if every element in the array ends in the passed in character, return true. Otherwise
		return false.
		Use the students array and the character "e" as arguments when testing.
	*/

	console.log(students.every(i =>	{return (typeof i === "string")})) // true

	function checkAllStringsEnding (arrayName, character)
	{
		if (arrayName.length !== 0)
		{
			// console.log("Yay")
			if (arrayName.every(i =>{return (typeof i === "string")}) )
			{
				// console.log("Yay")
				if (typeof character == "string")
				{
					// console.log("Yay")
					if (character.length === 1)
					{
						// console.log("Yay")
						console.log( arrayName.every(i =>{return (i[i.length-1]) === character}))
					}
					else {console.log("error - 2nd argurment must be a single character")}
				}
				else {console.log("error - 2nd argurment must be of data type: STRING")}
			}

			else { console.log("error - all array elements must be STRINGS")}
		}
		else { console.log("error - passed in array must NOT be empty")}
	}
		

	console.log("#4: checkAllStringsEnding")
	checkAllStringsEnding (students, "e");
	checkAllStringsEnding ([], "e");
	checkAllStringsEnding (["Jane", 02], "e");
	checkAllStringsEnding (students, "el");
	checkAllStringsEnding (students, 4);

	console.log("#4 - checking")
	// Check if code is working
	const studerndWithE = ["Joe", "Jane", "Janae"]
	checkAllStringsEnding (studerndWithE, "e");

	/* SCRATCH
	
		/*{
			if (arrayName.every(x =>
						{
							typeof x === "string"
						})
				)
				{
					if (typeof character == "string")
					{
						if (character.length = 1)
						{
							arrayName.every(y =>
							{
								console.log(y[y.length -1])
							})
						}
						else
							{	console.log("2nd argument must be a single character")}
					}
					else
						{ console.log("error - 2nd argument must be of data type STRING")}
				}
			else {console.log("error - all array elements must be strings")}
		}
		else 
		{
			console.log()
		}
	}*/

// STRETCH GOALS!

	/*
		5. Create a function named stringLengthSorter that will take in an array of
		strings as its argument and sort its elements in an ascending order based
		on their lengths. If at least one element is not a string, return "error - all
		array elements must be strings". Otherwise, return the sorted array. Use
		the students array to test.
	*/

	function stringLengthSorter (arrayName)
	{
		if (arrayName.every(i =>{return (typeof i === "string")}) )
		{
			// console.log("Yay")
			let ascendingSort = arrayName.sort((a,b) => a.length - b.length);
			console.log(ascendingSort);

		}
		else { console.log("ALL array elements must be STRINGS")}
	}
	
	console.log("#5: stringLengthSorter")
	stringLengthSorter (students);
	stringLengthSorter ([037, "John", 099])


	/*
		6. Create a function named startsWithCounter that will take in an array of strings and a
		single character. The function will do the ff:
		● if array is empty, return "error - array must NOT be empty"
		● if at least one array element is NOT a string, return "error - all array elements must be strings"
		● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		● if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
		● return the number of elements in the array that start with the character argument,must be case-insensitive
		Use the students array and the character "J" as arguments when testing.
	*/

	function startsWithCounter (arrayName, firstLetter)
	{
		if (arrayName.length !== 0)
		{
			// console.log("Yay")
			if (arrayName.every(i =>{return (typeof i === "string")}) )
			{
				// console.log("Yay")
				if (typeof firstLetter == "string")
				{
					// console.log("Yay")
					if (firstLetter.length === 1)
					{
						// console.log("Yay")
						let newArray = arrayName.filter(i => {return (i[0].toLowerCase()) === firstLetter.toLowerCase()})
						//console.log(newArray)
						console.log(newArray.length)
					}
					else {console.log("error - 2nd argurment must be a single character")}
				}
				else {console.log("error - 2nd argurment must be of data type: STRING")}
			}

			else { console.log("error - all array elements must be STRINGS")}
		}
		else { console.log("error - passed in array must NOT be empty")}
	}

	console.log("#6")
	startsWithCounter(students, "j")


	/*
		7. Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:
		● if array is empty, return "error - array must NOT be empty"
		● if at least one array element is NOT a string, return "error - all array elements must be strings"
		● if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		● return a new array containing all elements of the array argument that have the string argument in it, must be case-insensitive
		Use the students array and the character "jo" as arguments when testing.

	*/

	function likeFinder (arrayName, character)
	{
		if (arrayName.length !== 0)
		{
			// console.log("Yay")
			if (arrayName.every(i =>{return (typeof i === "string")}) )
			{
				// console.log("Yay")
				if (typeof character == "string")
				{
					let filteredArray = arrayName.filter(function(name)
								{
									return name.toLowerCase().includes(character);
								});
						console.log (filteredArray);

				}
				else {console.log("error - 2nd argurment must be of data type: STRING")}
			}

			else { console.log("error - all array elements must be STRINGS")}
		}
		else { console.log("error - passed in array must NOT be empty")}
	}

	console.log("#7")
	likeFinder (students, "jo")
	likeFinder (students, "ry")


	/*
		8. Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
	*/

	function randomPicker (arrayName)
	{
		let random = Math.floor(Math.random() * arrayName.length);
		console.log(random, arrayName[random]);
	}

	console.log("#8")
	randomPicker (students);
	randomPicker (students);
	randomPicker (students);
	randomPicker (students);
	randomPicker (students);
	